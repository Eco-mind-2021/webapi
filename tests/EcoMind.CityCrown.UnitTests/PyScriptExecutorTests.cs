using System;
using System.Threading;
using EcoMind.CityCrown.Neural;
using Xunit;

namespace EcoMind.CityCrown.UnitTests
{
    public class PyScriptExecutorTests
    {
        [Fact]
        public void Run()
        {
            var exec = new PyScriptExecutor();
            exec.Run("detect", "dataset/dogs2", CancellationToken.None);
        }
    }
}