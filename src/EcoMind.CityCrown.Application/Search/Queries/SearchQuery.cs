using System.Collections.Generic;
using EcoMind.CityCrown.Infrastructure.Models;
using MediatR;

namespace EcoMind.CityCrown.Application.Search.Queries
{
    public record SearchQuery(string SearchPhrase, SearchFilter Filter) : IRequest<IEnumerable<PhotoDalDto>>;
}