using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Application.Search.Queries;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Infrastructure.Repositories;
using MediatR;

namespace EcoMind.CityCrown.Application.Search.Handlers
{
    internal class SearchQueryHandler : IRequestHandler<SearchQuery, IEnumerable<PhotoDalDto>>
    {
        private readonly ISearchRepository _searchRepository;

        public SearchQueryHandler(ISearchRepository searchRepository) => _searchRepository = searchRepository ?? throw new ArgumentNullException(nameof(searchRepository));

        /// <inheritdoc />
        public async Task<IEnumerable<PhotoDalDto>> Handle(SearchQuery request, CancellationToken cancellationToken)
        {
            var photos =  await _searchRepository.SearchPhotos(request.SearchPhrase, request.Filter, cancellationToken);
            return photos;
        }
    }
}