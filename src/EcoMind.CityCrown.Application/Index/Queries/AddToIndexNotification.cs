using System.Collections.Generic;
using EcoMind.CityCrown.Neural.Models.CameraDetection;
using MediatR;

namespace EcoMind.CityCrown.Application.Index.Queries
{
    public record AddToIndexNotification(IEnumerable<CameraShot> Shoots) : INotification
    {
    }
}