using System.Collections.Generic;
using EcoMind.CityCrown.Infrastructure.Models;
using MediatR;

namespace EcoMind.CityCrown.Application.Index.Queries
{
    public record ConvertDatabaseToCSVQuery() : IRequest<List<PhotoDalDto>>;
}