using System.Collections.Generic;
using MediatR;

namespace EcoMind.CityCrown.Application.Index.Queries
{
    public record GetIndexListQuery : IRequest<List<string>>;
}