using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Application.Index.Queries;
using EcoMind.CityCrown.Domain;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Infrastructure.Repositories;
using MapsterMapper;
using MediatR;
using MoreLinq;
using MoreLinq.Extensions;

namespace EcoMind.CityCrown.Application.Index.Handlers
{
    internal class AddToIndexNotificationHandler : INotificationHandler<AddToIndexNotification>
    {
        private readonly IIndexRepository _indexRepository;
        private readonly IMapper _mapper;

        public AddToIndexNotificationHandler(IIndexRepository indexRepository, IMapper mapper)
        {
            _indexRepository = indexRepository ?? throw new ArgumentNullException(nameof(indexRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <inheritdoc />
        public async Task Handle(AddToIndexNotification notification, CancellationToken cancellationToken)
        {
            var shoots = notification.Shoots;

            var cameras = ToHashSetExtension
                .ToHashSet(MoreEnumerable
                    .DistinctBy(shoots.Select(c => c.Camera), s => s.Id));

            await _indexRepository.AddCameras(cameras, cancellationToken);

            foreach (var shot in shoots)
            {
                var aDog = shot.Objects.FirstOrDefault(s => s.Type == DetectedObjectType.Dog);

                AverageColorRGBDalDto averageColor = default;
                if (aDog?.Color?.Average != default)
                    averageColor = _mapper.Map<AverageColorRGBDalDto>(aDog.Color.Average);

                ImageCoordinatesDalDto coordinates = default;
                if (aDog?.Coordinates != default)
                    coordinates = _mapper.Map<ImageCoordinatesDalDto>(aDog.Coordinates);

                var relativePathStartIndex = shot.Path.IndexOf("/camera/", StringComparison.OrdinalIgnoreCase);
                var relativePath = shot.Path[relativePathStartIndex..];

                var photo = new PhotoDalDto
                {
                    Path = relativePath,
                    IsAnimal = shot.Animal == 1,
                    FileName = shot.Filename,
                    DetectedObjectKind = aDog == default ? DetectedObjectType.Human : DetectedObjectType.Dog,
                    OwnerClass = aDog?.Owner?.Class ?? 0,
                    Tail = aDog?.Tail?.Class ?? 0,
                    Color = aDog?.Color?.Class ?? 0,
                    CameraId = shot.Camera.Id,
                    CameraTime = shot.Camera.Datetime,
                    AverageColorRgb = averageColor,
                    ImageCoordinates = coordinates,
                    Confidence = aDog?.Confidence ?? default,
                    Breed = aDog?.Breed
                };
                await _indexRepository.Add(photo, cancellationToken);
            }

            await _indexRepository.SaveChanges(cancellationToken);
        }
    }
}