using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Application.Index.Queries;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Infrastructure.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace EcoMind.CityCrown.Application.Index.Handlers
{
    internal class ConvertDatabaseToCSVQueryHandler : IRequestHandler<ConvertDatabaseToCSVQuery, List<PhotoDalDto>>
    {
        private readonly IIndexRepository _indexRepository;

        public ConvertDatabaseToCSVQueryHandler(IIndexRepository indexRepository) => _indexRepository = indexRepository ?? throw new ArgumentNullException(nameof(indexRepository));

        /// <inheritdoc />
        public Task<List<PhotoDalDto>> Handle(ConvertDatabaseToCSVQuery request, CancellationToken cancellationToken)
        {
            var allEntries = _indexRepository.GetAll().ToListAsync(cancellationToken);
            return allEntries;
        }
    }
}