﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EcoMind.CityCrown.Application
{
    public static class Dependencies
    {
        public static IServiceCollection AddApplicationLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(typeof(Dependencies).Assembly);
            return services;
        }
    }
}