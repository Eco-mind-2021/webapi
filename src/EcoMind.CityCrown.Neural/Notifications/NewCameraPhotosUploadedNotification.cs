using System.Collections.Generic;
using MediatR;

namespace EcoMind.CityCrown.Neural.Notifications
{
    public record NewCameraPhotosUploadQuery(string AbsPath) : IRequest<List<string>>;
}