﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EcoMind.CityCrown.Neural
{
    public class PyScriptExecutor
    {
        public async Task<string> Run(string scriptName, string photosToProcessAbsPath, CancellationToken cancellationToken)
        {
            var baseDir = $"{Environment.CurrentDirectory}/Python";
            
            var execFile = $"{baseDir}/{scriptName}.py";
            var args = $"{execFile} --weights {baseDir}/runs/train/yolov5s/best.pt --source {photosToProcessAbsPath} --by-finish";

            using (var p = new Process())
            {
                p.StartInfo = new ProcessStartInfo(@"python", args)
                {
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };
                p.EnableRaisingEvents = true;
                p.ErrorDataReceived += PyError;
                p.OutputDataReceived += PyOutput;
                
                if (!p.Start())
                    throw new Exception("Process not started");

                try
                {
                    await p.WaitForExitAsync(cancellationToken);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
                var output = await p.StandardOutput.ReadToEndAsync();

                if (p.ExitCode != 0)
                {
                    var err = await p.StandardError.ReadToEndAsync();
                    throw new Exception($"Process {p.StartInfo.FileName} exited with code {p.ExitCode} : {err}");
                }

                return output;
            }
        }

        private void PyError(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        private void PyOutput(object sender, DataReceivedEventArgs e)
        {
            Console.Write(e.Data);
        }
    }
}