﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EcoMind.CityCrown.Neural
{
    public static class Dependencies
    {
        public static IServiceCollection AddNeuralLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(typeof(Dependencies).Assembly);
            return services;
        }
    }
}