using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Neural.Notifications;
using MediatR;

namespace EcoMind.CityCrown.Neural.Handlers
{
    internal class NewCameraPhotosUploadQueryHandler : IRequestHandler<NewCameraPhotosUploadQuery, List<string>>
    {
        private const int MaxPhotosPerChunk = 40;
        private const int MaxThreadsToProcess = 6;
        private static readonly Semaphore Semaphore = new(MaxThreadsToProcess, MaxThreadsToProcess);

        /// <inheritdoc />
        public Task<List<string>> Handle(NewCameraPhotosUploadQuery request, CancellationToken cancellationToken)
        {
            var results = new List<string>();
            var tasks = new List<Task>();
            var stopWatch = new Stopwatch();
            
            try
            {
                stopWatch.Start();
                foreach (var chunksPath in SplitToChunks(request.AbsPath))
                {
                    Semaphore.WaitOne(TimeSpan.FromHours(1));
                    var tsk = Task.Run(async () =>
                    {
                        var pyExecutor = new PyScriptExecutor();
                        var consoleLog = await pyExecutor.Run("detect", chunksPath, cancellationToken);
                        results.Add(consoleLog);
                        Semaphore.Release();
                        Console.WriteLine("Semaphore released");
                    }, cancellationToken);

                    tasks.Add(tsk);
                }

                Console.WriteLine($"{tasks.Count} scheduled");
                Task.WaitAll(tasks.ToArray(), cancellationToken);

                Console.WriteLine($"Total time to process {results.Count} threads: {stopWatch.Elapsed.TotalMinutes} minutes");
                stopWatch.Reset();
                
                return Task.FromResult(results);
            }
            catch (Exception)
            {
                Semaphore.Release();
                throw;
            }
        }

        private IEnumerable<string> SplitToChunks(string absPath)
        {
            var files = Directory.GetFiles(absPath);
            var cnt = files.Length;
            var chunks = Math.Ceiling((double)cnt / MaxPhotosPerChunk);
            var dirs = new List<string>();

            for (var i = 0; i < chunks; i++)
            {
                var filesToCopy = files.Skip(MaxPhotosPerChunk * i).Take(MaxPhotosPerChunk);
                var directory = Directory.CreateDirectory(Path.Combine(absPath, i.ToString()));
                CopyFilesToSubDir(filesToCopy, absPath, directory);
                dirs.Add(directory.FullName);
            }

            return dirs;
        }

        private void CopyFilesToSubDir(IEnumerable<string> filesToCopy, string sourcePath, DirectoryInfo directory)
        {
            foreach (var file in filesToCopy)
            {
                var fileName = Path.GetFileName(file);
                File.Move(file, Path.Combine(directory.FullName, fileName));
            }
        }
    }
}