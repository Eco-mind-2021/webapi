namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class Palette
    {
        public double Part { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}