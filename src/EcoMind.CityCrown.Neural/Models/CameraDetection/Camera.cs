namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class Camera
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Datetime { get; set; }

        /// <inheritdoc />
        public override int GetHashCode() => Id.GetHashCode();
    }
}