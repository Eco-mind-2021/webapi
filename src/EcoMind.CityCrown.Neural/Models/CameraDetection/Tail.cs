using EcoMind.CityCrown.Domain;

namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class Tail
    {
        public TailCode Class { get; set; }
        public double Length { get; set; }
    }
}