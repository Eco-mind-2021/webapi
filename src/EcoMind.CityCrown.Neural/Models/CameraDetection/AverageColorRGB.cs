namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class AverageColorRGB
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}