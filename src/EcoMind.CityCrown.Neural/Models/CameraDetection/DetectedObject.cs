using System.Collections.Generic;
using EcoMind.CityCrown.Domain;

namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class DetectedObject
    {
        public DetectedObjectType Type { get; set; }
        public double Confidence { get; set; }
        public ImageCoordinates Coordinates { get; set; }
        public Tail Tail { get; set; }
        public Color Color { get; set; }
        public Owner Owner { get; set; }
        public List<int> Breed { get; set; }
    }
}