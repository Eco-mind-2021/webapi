using System.Collections.Generic;
using EcoMind.CityCrown.Domain;

namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class Color
    {
        public ColorCode Class { get; set; }
        public AverageColorRGB Average { get; set; }
        public List<Palette> Palette { get; set; }
    }
}