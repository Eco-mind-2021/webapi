namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class ImageCoordinates
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }
}