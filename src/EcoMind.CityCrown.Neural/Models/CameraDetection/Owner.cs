namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class Owner
    {
        public int Class { get; set; }
        public double Distance { get; set; }
    }
}