using System.Collections.Generic;

namespace EcoMind.CityCrown.Neural.Models.CameraDetection
{
    public class CameraShot
    {
        public string Path { get; set; }
        public string Filename { get; set; }
        public int Animal { get; set; }
        public Camera Camera { get; set; }
        public List<DetectedObject> Objects { get; set; }
    }
}