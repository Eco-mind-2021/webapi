using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace EcoMind.CityCrown.Infrastructure.Configurations
{
    public static class JsonbExtension
    {
        public static PropertyBuilder<TProperty> IsJsonb<TProperty>(this PropertyBuilder<TProperty> propertyBuilder)
            where TProperty : class
        {
            propertyBuilder.HasConversion(data => JsonConvert.SerializeObject(data), data => JsonConvert
                .DeserializeObject<TProperty>(data)).HasColumnType("jsonb");
            return propertyBuilder;
        }
    }

}