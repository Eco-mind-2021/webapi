using EcoMind.CityCrown.Neural.Models.CameraDetection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EcoMind.CityCrown.Infrastructure.Configurations
{
    internal class CameraConfiguration : IEntityTypeConfiguration<Camera>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<Camera> builder)
        {
            builder.ToTable("Cameras");
            builder.HasKey(s => s.Id);
            builder.Ignore(s => s.Datetime);
        }
    }
}