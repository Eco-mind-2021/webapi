using EcoMind.CityCrown.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EcoMind.CityCrown.Infrastructure.Configurations
{
    internal class PhotoDalDtoConfiguration : IEntityTypeConfiguration<PhotoDalDto>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<PhotoDalDto> builder)
        {
            builder.ToTable("Photos");
            builder.HasKey(s => s.Id);
            builder.HasIndex(s => s.FileName);
            builder.HasIndex(s => s.Tail);
            builder.HasIndex(s => s.OwnerClass);
            builder.HasIndex(s => s.DetectedObjectKind);
            builder.HasIndex(s => s.Color);
            builder.Property(s => s.AverageColorRgb).IsJsonb();
            builder.Property(s => s.ImageCoordinates).IsJsonb();
            builder.Property(s => s.Breed).HasColumnType("integer[]");
        }
    }
}