using System.Linq;
using System.Reflection;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Neural.Models.CameraDetection;
using Microsoft.EntityFrameworkCore;

namespace EcoMind.CityCrown.Infrastructure
{
    public class PetFinderDbContext : DbContext
    {
        private static bool _isFirstRun = true;
        private static readonly object Locker = new();

        public DbSet<PhotoDalDto> Photos { get; set; }
        public DbSet<Camera> Cameras { get; set; }

        public PetFinderDbContext(DbContextOptions<PetFinderDbContext> options) : base(options)
        {
            if (_isFirstRun) Automigrate();
        }

        private void Automigrate()
        {
            lock (Locker)
            {
                if (_isFirstRun)
                {
                    Database.EnsureCreated();
                    _isFirstRun = false;
                }
            }
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetAssembly(typeof(PetFinderDbContext)));
            base.OnModelCreating(modelBuilder);
        }

        /// <inheritdoc />
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableDetailedErrors();
            base.OnConfiguring(optionsBuilder);
        }
    }
}