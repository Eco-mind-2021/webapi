﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EcoMind.CityCrown.Infrastructure.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cameras",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Adress = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cameras", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CameraTime = table.Column<string>(type: "text", nullable: true),
                    Path = table.Column<string>(type: "text", nullable: true),
                    DetectedObjectKind = table.Column<int>(type: "integer", nullable: false),
                    Confidence = table.Column<double>(type: "double precision", nullable: false),
                    OwnerClass = table.Column<int>(type: "integer", nullable: false),
                    Tail = table.Column<int>(type: "integer", nullable: false),
                    Color = table.Column<int>(type: "integer", nullable: false),
                    ImageCoordinates = table.Column<string>(type: "jsonb", nullable: true),
                    AverageColorRgb = table.Column<string>(type: "jsonb", nullable: true),
                    FileName = table.Column<string>(type: "text", nullable: true),
                    IsAnimal = table.Column<bool>(type: "boolean", nullable: false),
                    CameraId = table.Column<string>(type: "text", nullable: true),
                    Breed = table.Column<List<int>>(type: "integer[]", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Photos_Cameras_CameraId",
                        column: x => x.CameraId,
                        principalTable: "Cameras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Photos_CameraId",
                table: "Photos",
                column: "CameraId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_Color",
                table: "Photos",
                column: "Color");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_DetectedObjectKind",
                table: "Photos",
                column: "DetectedObjectKind");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_FileName",
                table: "Photos",
                column: "FileName");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_OwnerClass",
                table: "Photos",
                column: "OwnerClass");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_Tail",
                table: "Photos",
                column: "Tail");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.DropTable(
                name: "Cameras");
        }
    }
}
