using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Neural.Models.CameraDetection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EcoMind.CityCrown.Infrastructure.Repositories
{
    internal class IndexRepository : IIndexRepository
    {
        protected readonly PetFinderDbContext PetFinderDbContext;

        public IndexRepository(PetFinderDbContext petFinderDbContext)
        {
            PetFinderDbContext =
                petFinderDbContext ?? throw new ArgumentNullException(nameof(petFinderDbContext));
        }

        /// <inheritdoc />
        public async Task AddCameras(HashSet<Camera> cameras, CancellationToken cancellationToken)
        {
            foreach (var camera in cameras)
            {
                var isCameraExits =
                    await PetFinderDbContext.Cameras.AnyAsync(s => s.Id == camera.Id, cancellationToken: cancellationToken);

                if (!isCameraExits)
                    await PetFinderDbContext.AddAsync(camera, cancellationToken);
            }
        }

        public ValueTask<EntityEntry<PhotoDalDto>> Add(PhotoDalDto photo, CancellationToken cancellationToken) =>
            PetFinderDbContext.Photos.AddAsync(photo, cancellationToken);

        public Task SaveChanges(CancellationToken cancellationToken) => PetFinderDbContext.SaveChangesAsync(cancellationToken);

        /// <inheritdoc />
        public IQueryable<PhotoDalDto> GetAll() => PetFinderDbContext.Photos.Include(s => s.Camera);
    }
}