using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace EcoMind.CityCrown.Infrastructure.Repositories
{
    internal class SearchRepository : ISearchRepository
    {
        private readonly PetFinderDbContext _petFinderDbContext;

        public SearchRepository(PetFinderDbContext petFinderDbContext)
        {
            _petFinderDbContext = petFinderDbContext ?? throw new ArgumentNullException(nameof(petFinderDbContext));
        }

        public async Task<IEnumerable<PhotoDalDto>> SearchPhotos(string phrase, SearchFilter filter,
            CancellationToken cancellationToken)
        {
            var query = _petFinderDbContext.Photos.Include(s=>s.Camera)
                .AsQueryable();

            if (filter != default)
            {
                if (filter.Color.HasValue)
                    query = query.Where(s => s.Color == filter.Color);
                if (!string.IsNullOrEmpty(filter.FileName))
                    query = query.Where(s => s.FileName == filter.FileName);
                if (filter.OwnerClass.HasValue)
                    query = query.Where(s => s.OwnerClass == filter.OwnerClass);
                if (filter.Tail.HasValue)
                    query = query.Where(s => s.Tail == filter.Tail);
                if (filter.Breed.HasValue)
                    query = query.Where(s => s.Breed.Contains(filter.Breed.Value));
                if (filter.DetectedObjectKind.HasValue)
                    query = query.Where(s => s.DetectedObjectKind == filter.DetectedObjectKind);
            }

            var result = await query.ToListAsync(cancellationToken);
            return result;
        }
    }
}