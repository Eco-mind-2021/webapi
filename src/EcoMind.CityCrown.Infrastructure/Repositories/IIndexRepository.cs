using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Neural.Models.CameraDetection;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EcoMind.CityCrown.Infrastructure.Repositories
{
    public interface IIndexRepository
    {
        Task AddCameras(HashSet<Camera> cameras, CancellationToken cancellationToken);
        ValueTask<EntityEntry<PhotoDalDto>> Add(PhotoDalDto photo, CancellationToken cancellationToken);
        Task SaveChanges(CancellationToken cancellationToken);
        IQueryable<PhotoDalDto> GetAll();
    }
}