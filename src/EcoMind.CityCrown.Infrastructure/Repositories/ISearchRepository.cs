using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Infrastructure.Models;

namespace EcoMind.CityCrown.Infrastructure.Repositories
{
    public interface ISearchRepository
    {
        public Task<IEnumerable<PhotoDalDto>> SearchPhotos(string phrase, SearchFilter filter,
            CancellationToken cancellationToken);
    }
}