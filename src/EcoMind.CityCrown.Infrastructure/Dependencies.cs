﻿using System;
using EcoMind.CityCrown.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace EcoMind.CityCrown.Infrastructure
{
    public static class Dependencies
    {
        public static IServiceCollection AddDalLayer(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("postgres");
            if (string.IsNullOrEmpty(connectionString))
                throw new MissingFieldException("Connection string with name 'postgres' is required");

            services.AddDbContext<PetFinderDbContext>(builder =>
            {
                //builder.UseInMemoryDatabase("PetFinder");
                builder.UseNpgsql(new NpgsqlConnection(connectionString));
            });


            services.AddScoped<ISearchRepository, SearchRepository>();
            services.AddScoped<IIndexRepository, IndexRepository>();
            return services;
        }
    }
}