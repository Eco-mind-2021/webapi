using EcoMind.CityCrown.Domain;

namespace EcoMind.CityCrown.Infrastructure.Models
{
    public class SearchFilter
    {
        public int? OwnerClass { get; set; }
        public DetectedObjectType? DetectedObjectKind { get; set; }
        public TailCode? Tail { get; set; }
        public ColorCode? Color { get; set; }
        public int? Breed { get; set; }
        public string FileName { get; set; }
    }
}