using System;
using System.Collections.Generic;
using EcoMind.CityCrown.Domain;
using EcoMind.CityCrown.Neural.Models.CameraDetection;

namespace EcoMind.CityCrown.Infrastructure.Models
{
    public class PhotoDalDto
    {
        public long Id { get; set; }
        public string CameraTime { get; set; }
        public string Path { get; set; }
        public DetectedObjectType DetectedObjectKind { get; set; }
        public double Confidence { get; set; }
        public int OwnerClass { get; set; }
        public TailCode Tail { get; set; }
        public ColorCode Color { get; set; }
        public ImageCoordinatesDalDto ImageCoordinates { get; set; }
        public AverageColorRGBDalDto AverageColorRgb { get; set; }
        public string FileName { get; set; }
        public bool IsAnimal { get; set; }

        public virtual Camera Camera { get; set; }
        public string CameraId { get; set; }
        public List<int> Breed { get; set; }
    }
}