namespace EcoMind.CityCrown.Infrastructure.Models
{
    public class AverageColorRGBDalDto
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}