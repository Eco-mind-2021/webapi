namespace EcoMind.CityCrown.Infrastructure.Models
{
    public class ImageCoordinatesDalDto
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }
}