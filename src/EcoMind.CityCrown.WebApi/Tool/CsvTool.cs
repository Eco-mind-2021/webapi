using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using EcoMind.CityCrown.Neural.Models.CameraDetection;
using EcoMind.CityCrown.WebApi.Controllers;
using EcoMind.CityCrown.WebApi.Models.Responses.Import;

namespace EcoMind.CityCrown.WebApi.Tools
{
    public class CsvTool
    {
        public static async Task<string> GetCSV(ImportResultCsvResponse csvDataToExport)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture);

            string csvText;
            using (var writer = new StringWriter())
            using (var csv = new CsvWriter(writer, config))
            {
                csv.Context.RegisterClassMap<CsvExportHeader>();
                csv.WriteHeader<ShotEntry>();
                await csv.NextRecordAsync();

                foreach (var record in csvDataToExport.Shots)
                {
                    csv.WriteRecord(record);
                    await csv.NextRecordAsync();
                }

                await csv.FlushAsync();
                await writer.FlushAsync();
                csvText = writer.ToString();
            }

            return csvText;
        }
        
        
    }
}