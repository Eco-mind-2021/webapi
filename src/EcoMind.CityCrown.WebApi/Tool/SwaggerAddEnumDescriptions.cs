using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace EcoMind.CityCrown.WebApi.Tools
{
    public class XEnumNamesSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            var typeInfo = context.Type;
            if (typeInfo.IsEnum)
            {
                var names = Enum.GetNames(context.Type).Select(name => new OpenApiString(name));

                var dataMemberAttrs = GetDataMemberAttrsForValues(context.Type);

                var xEnumNames = new OpenApiArray();
                xEnumNames.AddRange(names);
                schema.Extensions.Add("x-enumNames", xEnumNames);

                if (dataMemberAttrs != null)
                {
                    var dataMemberAttrsName = new OpenApiArray();
                    dataMemberAttrsName.AddRange(dataMemberAttrs
                        .Select((k, v) => new OpenApiString(k.Value)));
                    schema.Extensions.Add("x-enumTitles", dataMemberAttrsName);
                }
            }
            else if (schema.Enum?.Any() != false)
            {
                var names = schema.Enum.Select(n => new OpenApiString(n.ToString()));
                var arr = new OpenApiArray();
                arr.AddRange(names);

                if (arr.Count > 0)
                    schema.Extensions.Add("x-enumNames", arr);
            }
            else if (typeInfo.IsGenericType && !schema.Extensions.ContainsKey("x-enumNames"))
            {
                foreach (var genericArgumentType in typeInfo.GetGenericArguments())
                {
                    if (genericArgumentType.IsEnum)
                    {
                        var names = Enum.GetNames(genericArgumentType).Select(name => new OpenApiString(name));

                        var arr = new OpenApiArray();
                        arr.AddRange(names);

                        if (!schema.Extensions.ContainsKey("x-enumNames") && arr.Count > 0)
                            schema.Extensions.Add("x-enumNames", arr);
                    }
                }
            }
        }

        private static Dictionary<string, string> GetDataMemberAttrsForValues(Type enumType)
        {
            var memberInfos = enumType.GetMembers();
            var dmAttrNames = new Dictionary<string, string>();

            foreach (var memberInfo in memberInfos)
            {
                var dataMemberAttrInfo = memberInfo.GetCustomAttribute(typeof(DataMemberAttribute), false);

                var attributeNameValue = ((DataMemberAttribute)dataMemberAttrInfo)?.Name;
                if (attributeNameValue != null) dmAttrNames.Add(memberInfo.Name, attributeNameValue);
            }

            return dmAttrNames.Any() ? dmAttrNames : default;
        }
    }
}