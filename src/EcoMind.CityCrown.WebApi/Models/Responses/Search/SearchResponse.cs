using System.Collections.Generic;
using EcoMind.CityCrown.Infrastructure.Models;

namespace EcoMind.CityCrown.WebApi.Models.Responses.Search
{
    public class SearchResponse
    {
        public SearchResponse(IEnumerable<PhotoDalDto> response)
        {
            foreach (var photo in response)
            {
                var dto = new DetectedObjectResponse(photo);
                Photos.Add(dto);
            }
        }

        public List<DetectedObjectResponse> Photos { get; } = new();
    }
}