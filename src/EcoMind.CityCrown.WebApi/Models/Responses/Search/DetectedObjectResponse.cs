using System.Collections.Generic;
using System.Runtime.Serialization;
using EcoMind.CityCrown.Domain;
using EcoMind.CityCrown.Infrastructure.Models;

namespace EcoMind.CityCrown.WebApi.Models.Responses.Search
{
    [DataContract]
    public class DetectedObjectResponse
    {
        public DetectedObjectResponse(PhotoDalDto photo)
        {
            FileName = photo.Path;
            CameraId = photo.CameraId;
            CameraAddress = photo.Camera.Adress;
            CameraDatetime = photo.CameraTime;
            Tail = photo.Tail;
            Color = photo.Color;
            OwnerClass = photo.OwnerClass;
            Breed = photo.Breed;
        }

        [DataMember(Name = "fileName")] public string FileName { get; set; }
        [DataMember(Name = "cameraId")] public string CameraId { get; set; }
        [DataMember(Name = "cameraAddress")] public string CameraAddress { get; set; }
        [DataMember(Name = "cameraDatetime")] public string CameraDatetime { get; set; }
        [DataMember(Name = "tail")] public TailCode Tail { get; set; }
        [DataMember(Name = "color")] public ColorCode Color { get; set; }
        [DataMember(Name = "ownerClass")] public int OwnerClass { get; set; }
        [DataMember(Name = "breed")] public List<int> Breed { get; set; }
    }
}