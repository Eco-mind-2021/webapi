using System.Linq;
using EcoMind.CityCrown.Domain;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Neural.Models.CameraDetection;

namespace EcoMind.CityCrown.WebApi.Models.Responses.Import
{
    public class ShotEntry
    {
        public ShotEntry(CameraShot camEntry)
        {
            FileName = camEntry.Filename;
            IsAnimalThere = camEntry.Animal != 0;

            var dog = camEntry.Objects.FirstOrDefault(s => s.Type == DetectedObjectType.Dog);
            IsOwnerOverThere = dog?.Owner?.Class == 1;
            IsADog = dog != default;
            Color = (ColorCode)(dog?.Color?.Class ?? 0);
            Tail = (TailCode)(dog?.Tail?.Class ?? 0);
            Address = camEntry.Camera.Adress;
            CameraId = camEntry.Camera.Id;
        }

        public ShotEntry(PhotoDalDto camEntry)
        {
            FileName = camEntry.FileName;
            IsAnimalThere = camEntry.IsAnimal;

            IsOwnerOverThere = camEntry.OwnerClass != 0;
            IsADog = camEntry.DetectedObjectKind == DetectedObjectType.Dog;
            Color = camEntry.Color;
            Tail = camEntry.Tail;
            Address = camEntry.Camera.Adress;
            CameraId = camEntry.Camera.Id;
        }

        public string FileName { get; set; }
        public bool IsAnimalThere { get; set; }
        public bool IsADog { get; set; }
        public bool IsOwnerOverThere { get; set; }
        public ColorCode Color { get; set; }
        public TailCode Tail { get; set; }
        public string Address { get; set; }
        public string CameraId { get; set; }
    }
}