using System.Collections.Generic;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.Neural.Models.CameraDetection;

namespace EcoMind.CityCrown.WebApi.Models.Responses.Import
{
    public class ImportResultCsvResponse
    {
        public ImportResultCsvResponse(List<CameraShot> recognizedEntries)
        {
            foreach (var camEntry in recognizedEntries)
            {
                var shot = new ShotEntry(camEntry);
                Shots.Add(shot);
            }
        }

        public ImportResultCsvResponse(IEnumerable<PhotoDalDto> photos)
        {
            foreach (var camEntry in photos)
            {
                var shot = new ShotEntry(camEntry);
                Shots.Add(shot);
            }
        }

        public List<ShotEntry> Shots { get; set; } = new();
    }
}