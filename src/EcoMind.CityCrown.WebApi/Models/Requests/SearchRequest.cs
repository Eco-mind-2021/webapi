using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EcoMind.CityCrown.Infrastructure.Models;

namespace EcoMind.CityCrown.WebApi.Models.Requests
{
    public class SearchRequest
    {
        public SearchFilter Filter { get; set; }
    }
}