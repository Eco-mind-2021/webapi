using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EcoMind.CityCrown.Application;
using EcoMind.CityCrown.Infrastructure;
using EcoMind.CityCrown.Neural;
using EcoMind.CityCrown.WebApi.Tools;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace EcoMind.CityCrown.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        private static readonly Regex GenericRegex = new(@"(?<main>[A-Za-z\.]{15,})`\d\[\[(?<generic>[A-Za-z\.]+?),",
            RegexOptions.Compiled);

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin() 
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
            services.AddControllers(options => options.EnableEndpointRouting = false);
            services.AddSwaggerGen(options =>
            {
                options.ExampleFilters();
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Pet find v1", Version = "v1" });
                options.CustomOperationIds(c =>
                    $"{c.ActionDescriptor.RouteValues["controller"]}_{c.ActionDescriptor.RouteValues["action"]}");
                options.CustomSchemaIds(t =>
                {
                    if (!t.IsGenericType) return t.FullName?.ToString();
                    var match = GenericRegex.Match(t.FullName);

                    if (!match.Success)
                        return t.FullName?.ToString();

                    var groups = match.Groups;
                    var name = $"{GetTypeName(groups["main"].Value)}_of_{GetTypeName(groups["generic"].Value)}";
                    return name;
                });
                options.SchemaFilter<XEnumNamesSchemaFilter>();
                options.UseAllOfToExtendReferenceSchemas();
            });
            services.AddSwaggerGenNewtonsoftSupport();
            services.AddSwaggerExamplesFromAssemblies(Assembly.GetEntryAssembly());
            services.AddSwaggerExamples();

            services.AddDalLayer(Configuration);
            services.AddApplicationLayer(Configuration);
            services.AddNeuralLayer(Configuration);

            services.AddSingleton<IMapper, Mapper>();
            services.AddScoped<IMediator, Mediator>();
            services.AddDirectoryBrowser();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSentryTracing();
            app.UseRouting();
            app.UseAuthorization();
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader().WithExposedHeaders("*");
            });
            app.UseMvc();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireCors("AllowAll");
            });
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.EnableFilter();
                c.ShowExtensions();
                c.EnableTryItOutByDefault();
                c.DisplayOperationId();
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pet find v1");
                c.RoutePrefix = "";
                c.EnableValidator();
                c.DisplayRequestDuration();
            });
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine($"{Directory.GetCurrentDirectory()}", "camera")),
                RequestPath = "/camera"
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine($"{Directory.GetCurrentDirectory()}", "camera")),
                RequestPath = "/camera"
            });

        }

        private static string GetTypeName(string fullName) => fullName[(fullName.LastIndexOf('.') + 1)..];
    }
}