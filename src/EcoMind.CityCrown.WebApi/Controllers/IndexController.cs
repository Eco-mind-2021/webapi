using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO.Compression;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using EcoMind.CityCrown.Application.Index.Queries;
using EcoMind.CityCrown.Neural.Models.CameraDetection;
using EcoMind.CityCrown.Neural.Notifications;
using EcoMind.CityCrown.WebApi.Models.Responses.Import;
using EcoMind.CityCrown.WebApi.Tools;
using Newtonsoft.Json;

namespace EcoMind.CityCrown.WebApi.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class IndexController : ControllerBase
    {
        private readonly IMediator _mediator;

        public IndexController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Upload([Required] IFormFile file, CancellationToken cancellationToken)
        {
            if (file.Length <= 0) return BadRequest("input file is empty");
            var baseDir = $"{Environment.CurrentDirectory}/camera/{System.Diagnostics.Activity.Current.Id}";
            Directory.CreateDirectory(baseDir);

            using (var archive = new ZipArchive(file.OpenReadStream()))
            {
                var jpegs = archive.Entries
                    .Where(s => s.FullName == s.Name && s.FullName.EndsWith(".jpg"));
                foreach (var jpegEntry in jpegs)
                {
                    var path = Path.Combine(baseDir, jpegEntry.FullName);
                    jpegEntry.ExtractToFile(path);
                }
            }

            var responses = await _mediator.Send(new NewCameraPhotosUploadQuery(baseDir), cancellationToken);

            var recognizedEntries = new List<CameraShot>();
            foreach (var response in responses)
            {
                var pyResponses = JsonConvert.DeserializeObject<IEnumerable<CameraShot>>(response);
                recognizedEntries.AddRange(pyResponses);
            }

            var saveToDbTask = _mediator.Publish(new AddToIndexNotification(recognizedEntries), cancellationToken);

            var csvDataToExport = new ImportResultCsvResponse(recognizedEntries);
            var csvText = await CsvTool.GetCSV(csvDataToExport);

            await saveToDbTask;
            return Ok(csvText);
        }

        [HttpGet("export/csv")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCsvOfDataBase(CancellationToken cancellationToken)
        {
            var csvData = await _mediator.Send(new ConvertDatabaseToCSVQuery(), cancellationToken);
            
            var csvDataToExport = new ImportResultCsvResponse(csvData);
            var csvText = await CsvTool.GetCSV(csvDataToExport);
            
            return Ok(csvText);
        }
    }
}