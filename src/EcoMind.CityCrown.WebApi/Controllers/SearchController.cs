using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using EcoMind.CityCrown.Application.Search.Queries;
using EcoMind.CityCrown.Infrastructure.Models;
using EcoMind.CityCrown.WebApi.Models.Requests;
using EcoMind.CityCrown.WebApi.Models.Responses.Search;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace EcoMind.CityCrown.WebApi.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SearchController(IMediator mediator) => _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));

        [HttpGet]
        [ProducesResponseType(typeof(List<DetectedObjectResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Search([Required, FromQuery] SearchRequest searchRequest, CancellationToken
            cancellationToken)
        {
            var entries = await _mediator.Send(new SearchQuery(string.Empty, searchRequest.Filter),
                cancellationToken);

            if (!entries.Any())
                return NotFound("No entries found for search criteria");

            return Ok(new SearchResponse(entries).Photos);
        }
    }
}