using CsvHelper.Configuration;
using EcoMind.CityCrown.WebApi.Models.Responses.Import;

namespace EcoMind.CityCrown.WebApi.Controllers
{
    public sealed class CsvExportHeader : ClassMap<ShotEntry>
    {
        public CsvExportHeader()
        {
            Map(s => s.FileName).Index(0).Name("filename");
            Map(s => s.IsAnimalThere).Index(1).Name("is_animal_there").Convert(arg => (arg.Value.IsAnimalThere ? "1" : "0").ToString());
            Map(s => s.IsADog).Index(2).Name("is_it_a_dog").Convert(arg => (arg.Value.IsADog ? "1" : "0").ToString());
            Map(s => s.IsOwnerOverThere).Index(3).Name("is_the_owner_there").Convert(arg => (arg.Value.IsOwnerOverThere ? "1" : "0").ToString());
            Map(s => s.Color).Index(4).Name("color").Convert(args => ((int)args.Value.Color).ToString());
            Map(s => s.Tail).Index(5).Name("tail").Convert(args => ((int)args.Value.Tail).ToString());
            Map(s => s.Address).Index(6).Name("address");
            Map(s => s.CameraId).Index(7).Name("cam_id");
        }
    }
}