using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sentry;
using Sentry.AspNetCore.Grpc;

namespace EcoMind.CityCrown.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseSentry(o =>
                    {
                        o.Dsn = "https://ba18fda44d0b42109a64c386a6abbfcb@o1055203.ingest.sentry.io/6041145";
                        // When configuring for the first time, to see what the SDK is doing:
                        o.Debug = true;
                        o.AttachStacktrace = true;
                        o.DiagnosticLevel = SentryLevel.Debug;
                        o.DeduplicateMode = DeduplicateMode.All;
                        o.AutoSessionTracking = true;
                        o.StackTraceMode = StackTraceMode.Enhanced;
                        // Set traces_sample_rate to 1.0 to capture 100% of transactions for performance monitoring.
                        // We recommend adjusting this value in production.
                        o.TracesSampleRate = 1.0;
                    });
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureKestrel(serverOptions => serverOptions.Limits.MaxRequestBodySize = int.MaxValue);
                });
    }
}