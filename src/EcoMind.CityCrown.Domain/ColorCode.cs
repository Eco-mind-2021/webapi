namespace EcoMind.CityCrown.Domain
{
    public enum ColorCode
    {
        Unset = 0,
        Dark = 1,
        Light = 2,
        Colorful = 3
    }
}