namespace EcoMind.CityCrown.Domain
{
    public enum DetectedObjectType
    {
        Dog = 0,
        Human = 1,
    }
}