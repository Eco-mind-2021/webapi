namespace EcoMind.CityCrown.Domain
{
    public enum TailCode
    {
        Unset = 0,
        Short = 1,
        Long = 2
    }
}